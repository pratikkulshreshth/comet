import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { HomeComponent } from './home/home.component';
import { HomeHeaderComponent } from './home/home-header/home-header.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { HomeBodyComponent } from './home/home-body/home-body.component';
import { SecurityDetailsComponent } from './security-details/security-details.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';

const appRoutes: Routes = [
  { path: '', component: LandingComponent, },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: 'security', component: SecurityDetailsComponent },
      { path: 'personal', component: PersonalDetailsComponent },
      { path: 'contact', component: ContactDetailsComponent }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    HomeComponent,
    HomeHeaderComponent,
    SideMenuComponent,
    HomeBodyComponent,
    SecurityDetailsComponent,
    PersonalDetailsComponent,
    ContactDetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
