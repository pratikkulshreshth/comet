import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  isLoginModalOpen = false;
  isSignUpModalOpen = false;

  constructor() { }

  ngOnInit() {
  }

}
