import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  containerWidth = screen.availWidth - 1 + 'px';
  containerHeight = screen.availHeight - 1 + 'px';

  // containerHeight = '500px';
  // containerWidth = '700px';
}
